import os

FILE_NAME = "hosts_up.txt" # FILE PRO RUN ZGRABu
BASEDIR = os.path.abspath(os.path.dirname(__file__))
FILE_PATH = os.path.join(BASEDIR, FILE_NAME)
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'mydatabase.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

