
from flask import Flask, redirect, url_for, abort
from models import *

from flask_migrate import Migrate

from constants import *
from app import *
from flask import render_template, request
from scanner.host_scanner import initialize_hosts



app = Flask(__name__)


# Nastavení cesty k SQLite databázi v adresáři projektu
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS

# Připojení SQLAlchemy k aplikaci Flask
db.init_app(app)
migrate = Migrate(app, db)


# Vytvoření databáze na základě modelů
with app.app_context():
    db.create_all()


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/recent')
def recent():
    # Předpokládejme, že 'data' obsahuje data z databáze
    scans = Scan.query.all()
    return render_template('recent.html', scans=scans)

@app.route('/device/<id>')
def device(id):
    device = Device.query.get(id)
    if device is None:
        abort(404)  # Device not found
    zgrab_data = Zgrab.query.filter_by(device_id=id).first()
    return render_template('device.html', device=device, zgrab_data=zgrab_data)

@app.route('/scan', methods=['POST'])
def scan():
    target = request.form['ip_address']
    ports = request.form['port_range']
    scan_name = request.form['scan_name']
    thread_num = request.form['thread_num']
    vuln_scan_checkbox = request.form.get('vuln_scan')

    initialize_hosts(target, ports, scan_name, vuln_scan_checkbox, thread_num)

    return redirect(url_for('recent'))

@app.route('/port/<id>')
def port(id):
    port = Port.query.get(id)
    if port is None:
        abort(404)

    return render_template('port.html', port=port)

@app.template_filter('join_ports')
def join_ports(ports):
    return ', '.join(str(port.number) for port in ports)
    

if __name__ == '__main__':
    app.run(debug=True)

