from flask import render_template, request
from app.scanner.host_scanner import initialize_hosts

def configure_routes(app):

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/scan', methods=['POST'])
    def scan():
        target = request.form['ip_address']
        ports = request.form['port_range']
        scan_name = request.form['scan_name']
        hosts_up_string = initialize_hosts(target, ports, scan_name)
        return render_template('hosts.html', hosts_up_string=hosts_up_string)
