from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Scan(db.Model):
    __tablename__ = 'scans'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    name = db.Column(db.String, nullable=False)
    devices = db.relationship('Device', backref='scan', lazy=True)

class Device(db.Model):
    __tablename__ = 'devices'
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String, index=True, nullable=False)
    mac_address = db.Column(db.String, nullable=True)  # MAC address if available
    device_type = db.Column(db.String, nullable=True)  # General purpose, printer, etc.
    running = db.Column(db.String, nullable=True)  # OS running, e.g. Linux 2.6.X
    os_cpe = db.Column(db.String, nullable=True)  # OS CPE
    scan_id = db.Column(db.Integer, db.ForeignKey('scans.id'), nullable=False)
    ports = db.relationship('Port', backref='device', lazy=True)
    operation_systems = db.relationship('OperationSystem', backref='device', lazy=True)

class OperationSystem(db.Model):
    __tablename__ = 'os'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=True)  # OS name
    accuracy = db.Column(db.String, nullable=True)  # OS match accuracy
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)

class Port(db.Model):
    __tablename__ = 'port'
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer, nullable=False)
    state = db.Column(db.String, nullable=False)
    service_name = db.Column(db.String, nullable=True)  # Service name (ftp, http, etc.)
    product = db.Column(db.String, nullable=True)  # Product name (vsftpd, Apache httpd, etc.)
    version = db.Column(db.String, nullable=True)  # Version of the service
    extra_info = db.Column(db.String, nullable=True)  # Extra information if available
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)

class Vulnerability(db.Model):
    __tablename__ = 'vulnerabilities'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    score = db.Column(db.Float, nullable=False)
    url = db.Column(db.String)  # URL to the detailed vulnerability information
    exploit = db.Column(db.Boolean, nullable=True)  # True if exploit is available
    port_id = db.Column(db.Integer, db.ForeignKey('port.id'), nullable=False)


class Zgrab(db.Model):
    __tablename__ = 'zgrab'
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String, nullable=False)
    protocol = db.Column(db.String, nullable=False)
    is_s7 = db.Column(db.Boolean, nullable=False)
    system = db.Column(db.String, nullable=True)
    module = db.Column(db.String, nullable=True)
    plant_id = db.Column(db.String, nullable=True)
    copyright = db.Column(db.String, nullable=True)
    serial_number = db.Column(db.String, nullable=True)
    module_type = db.Column(db.String, nullable=True)
    reserved_for_os = db.Column(db.String, nullable=True)
    location = db.Column(db.String, nullable=True)
    module_id = db.Column(db.String, nullable=True)
    hardware = db.Column(db.String, nullable=True)
    firmware = db.Column(db.String, nullable=True)
    timestamp = db.Column(db.DateTime, nullable=False)
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)