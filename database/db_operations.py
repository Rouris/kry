
from models import *

def write_to_database(result, scan_id):
    try:
        for host in result.all_hosts():
            ip_address = result[host]['addresses'].get('ipv4', 'Unknown')
            hostname = result[host]['hostnames'][0]['name'] if result[host]['hostnames'] else 'Unknown'
            # Assuming 'device_type', 'running', and 'os_cpe' are direct outputs from Nmap:
            device_type = result[host]['hostnames'][0]['type'] if result[host]['hostnames'] else 'Unknown'  # Example static assignment
            

            new_device = Device(
                ip_address=ip_address,
                hostname=hostname,
                device_type=device_type,
                scan_id=scan_id
            )
            db.session.add(new_device)
            db.session.flush()

            # OS Matches
            for os_match in result[host].get('osmatch', []):
                new_os = OperationSystem(
                    name=os_match['name'],
                    accuracy=os_match['accuracy'],
                    device_id=new_device.id
                )
                db.session.add(new_os)

            # Port information
            for protocol in result[host].all_protocols():
                for port in result[host][protocol].keys():
                    service = result[host][protocol][port]
                    if service['state'] == 'open':
                        new_port = Port(
                            number=port,
                            state=service['state'],
                            service_name=service.get('name', 'Unknown'),
                            product=service.get('product', 'Unknown'),
                            version=service.get('version', 'Unknown'),
                            extra_info=service.get('extrainfo', 'Unknown'),
                            device_id=new_device.id
                        )
                        db.session.add(new_port)

        # Commit all the changes after all records are added
        db.session.commit()
    except Exception as e:
        # Rollback in case of any error during the process
        db.session.rollback()
        print(f"Error occurred: {e}")
    finally:
        # Close the session whether or not an error occurred
        db.session.close()




def write_to_database_zgrab(result, scan_id):

    ip_address = result.get('ip', None)
    if ip_address is not None:
        device = Device.query.filter_by(ip_address=ip_address, scan_id=scan_id).first()
        if device is not None:
            siemens_data = result.get('data', {}).get('siemens', {})
            zgrab_entry = Zgrab(
                status=siemens_data.get('status', 'Unknown'),
                protocol=siemens_data.get('protocol', 'Unknown'),
                is_s7=siemens_data.get('result', {}).get('is_s7', 'Unknown'),
                system=siemens_data.get('result', {}).get('system', 'Unknown'),
                module=siemens_data.get('result', {}).get('module', 'Unknown'),
                plant_id=siemens_data.get('result', {}).get('plant_id', 'Unknown'),
                copyright=siemens_data.get('result', {}).get('copyright', 'Unknown'),
                serial_number=siemens_data.get('result', {}).get('serial_number', 'Unknown'),
                module_type=siemens_data.get('result', {}).get('module_type', 'Unknown'),
                reserved_for_os=siemens_data.get('result', {}).get('reserved_for_os', 'Unknown'),
                location=siemens_data.get('result', {}).get('location', 'Unknown'),
                module_id=siemens_data.get('result', {}).get('module_id', 'Unknown'),
                hardware=siemens_data.get('result', {}).get('hardware', 'Unknown'),
                firmware=siemens_data.get('result', {}).get('firmware', 'Unknown'),
                timestamp=datetime.strptime(siemens_data.get('timestamp', 'Unknown'), "%Y-%m-%dT%H:%M:%S%z") if siemens_data.get('timestamp') else 'Unknown',
                device_id=device.id
            )
            db.session.add(zgrab_entry)
            db.session.commit()
            return device.id
    else:
        return None
    