import nmap


def run_nmap_scan(upHost, port_range, vuln_scan_checkbox):
    detailed_scanner = nmap.PortScanner()
    if vuln_scan_checkbox == 'on':
        detailed_options = f"-p {port_range} -sV -O --script vulners"
    else:
        detailed_options = f"-p {port_range} -sV -O"
    # Join the list of hosts that are up into a single string with spaces for the nmap command
    detailed_scanner.scan(hosts=upHost, arguments=detailed_options)
    return detailed_scanner