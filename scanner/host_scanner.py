import nmap
import os
import concurrent.futures
from models import *
from datetime import datetime
from constants import *
from scanner.vulnerability_scanner import *
from scanner.nmap_scanner import run_nmap_scan
from scanner.zgrab_scanner import run_zgrab
from database.db_operations import write_to_database




def initialize_hosts(ip_address, port_range, scan_name, vuln_scan_checkbox, thread_num):

    # ---------------------  ZJIŠTĚNÍ UP HOSTŮ --------------------

    nm = nmap.PortScanner()
   
    nm.scan(hosts=ip_address, arguments='-n -sP') # PING scan
    hosts_list = [(x, nm[x]['status']['state']) for x in nm.all_hosts()]
    
    def sort_by_last_octet(ip_address):
        return int(ip_address.split('.')[-1])

    # Extrakce IP adres z listu
    ip_addresses = [ip for ip, status in hosts_list if status == 'up']

    # Seřazení IP podle posledního oktetu
    ip_addresses = sorted(ip_addresses, key=sort_by_last_octet)

    #Zápis UP ip adres do souboru pro zgrab
    with open(FILE_PATH, 'w') as file:
        for ip in ip_addresses:
            file.write(ip + '\n')
    file.close()

    # Inicializace scanu v DB
    if len(ip_addresses) == 0:
        print("No hosts are up.")
        return None
    else:
        print("Hosts up:" + str(ip_addresses))
        scan_id = initialize_scan_db(scan_name)


    # ---------------------  SPUŠTĚNÍ NMAPU --------------------

    # Spuštění Nmapu na všech IP adresách pomocí multithreadingu
    with concurrent.futures.ThreadPoolExecutor( max_workers=int(thread_num)) as executor:
        unique_ip_addresses = set(ip_addresses)
        
        futures = {executor.submit(run_nmap_scan, ip, port_range, vuln_scan_checkbox) for ip in unique_ip_addresses}

        for future in concurrent.futures.as_completed(futures):
            scanner = future.result()
            #Zápis do databáze
            write_to_database(scanner, scan_id)

            #spuštění CVE scanu pokud je zvolená možnost
            if vuln_scan_checkbox == 'on':
                CVE_scan(scanner, 0, 0 , scan_id)


    # ---------------------  SPUŠTĚNÍ ZGRABU --------------------
    print("spuštění zgrab")
    run_zgrab(scan_id) 
    

    print("-------------------------------------------------------------------")
    return 0


def initialize_scan_db(name):
    timestamp = datetime.utcnow()
    new_scan = Scan(timestamp=timestamp, name=name)
    db.session.add(new_scan)
    db.session.flush()  # To get the scan ID for the foreign key relationship
    return new_scan.id
