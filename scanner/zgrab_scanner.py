
from database.db_operations import *
from models import *

import subprocess
import json


def run_zgrab(scan_id):
    result = subprocess.run(['./zgrab2', 'siemens', '-f', 'hosts_up.txt'], capture_output=True, text=True)

    if result.returncode != 0:
        print('zgrab2 command failed')
        return None

    json_results = [json.loads(line) for line in result.stdout.splitlines()]
        
    for result in json_results:
        status = result.get('data', {}).get('siemens', {}).get('status')
        if status == 'connection-timeout':
            pass
        elif status == 'success':
            write_to_database_zgrab(result, scan_id)



    return result